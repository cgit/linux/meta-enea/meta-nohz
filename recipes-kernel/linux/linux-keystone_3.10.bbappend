FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "\
        file://cfg/00039-nohz.cfg \
        file://patches/sched-nohz-add-debugfs-control-over-sched_tick_max_d.patch \
        file://patches/sched-nohz-Fix-overflow-error-in-scheduler_tick_max_.patch \
        file://patches/nohz-Drop-generic-vtime-obsolete-dependency-on-CONFI.patch \
        "

KERNEL_FEATURES += " \
        cfg/00039-nohz \
        "
