
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58"
INHIBIT_DEFAULT_DEPS = "1"

SRC_URI = "file://${PN} \
          "

FILES_${PN} += "${bindir}/${PN}"

RDEPENDS_${PN} = "ltp packagegroup-enea-rt-tools"

do_install () {
    install -D ${WORKDIR}/${PN} ${D}${bindir}/${PN}
}

# Only a script, no need for patch, configure, compile or build
do_patch[noexec] = "1"
do_configure[noexec] = "1"
do_compile[noexec] = "1"
do_build[noexec] = "1"
